/**
 * Copyright (C) 2014 Syddjurs Kommune
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.syddjurs.camel.component.taxon;

import java.util.Iterator;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.NoTypeConversionAvailableException;
import org.apache.camel.TypeConversionException;
import org.apache.camel.util.ObjectHelper;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public final class TaxonUtil {

	private TaxonUtil() {
	}

	public static String buildInputText(TaxonService taxonService, Exchange exchange) throws TypeConversionException, NoTypeConversionAvailableException {
		StringBuilder sb = new StringBuilder();
		// if subject is specified, use this as the first line.
		if (ObjectHelper.isNotEmpty(taxonService.getSubject())) {
			sb.append(taxonService.getSubject());
			sb.append("\r\n");
		}
		// if text parameter is supplied use that as input, otherwise use the
		// message body
		if (ObjectHelper.isNotEmpty(taxonService.getText())) {
			sb.append(taxonService.getText());
		} else {
			sb.append(exchange.getContext().getTypeConverter().mandatoryConvertTo(String.class, exchange.getIn().getBody()));
		}
		return sb.toString();
	}

	public static Exchange buildExchange(Exchange exchange, String taxonFullResult) {
		exchange.getIn().setHeader(TaxonConstants.TAXON_FULL_RESULT, taxonFullResult);
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode rootNode = objectMapper.readTree(taxonFullResult);
			
			String status = rootNode.get("status").get("status").asText();			
			exchange.getIn().setHeader(TaxonConstants.TAXON_STATUS, status);
			if( status.equals("OK"))
			{
				// extract values from the first result from Taxon. The first result is
				// always the highest ranked result.
				Iterator<Map.Entry<String, JsonNode>> taxonClasses = rootNode.get("classes").getFields();
				if( taxonClasses.hasNext())
				{
					Map.Entry<String, JsonNode> taxonClass = taxonClasses.next();
					exchange.getIn().setHeader(TaxonConstants.TAXON_CLASS, taxonClass.getKey());
					exchange.getIn().setHeader(TaxonConstants.TAXON_TITLE, taxonClass.getValue().get("title").asText());
					exchange.getIn().setHeader(TaxonConstants.TAXON_SCORE_TOTAL, taxonClass.getValue().get("scoreTotal").asInt());
					exchange.getIn().setHeader(TaxonConstants.TAXON_CONFIDENCE_COEFFICIENT, taxonClass.getValue().get("scoreConfidenceCoefficient").asInt());										
				}
			}
			if( status.equals("Error"))
			{
				exchange.getIn().setHeader(TaxonConstants.TAXON_ERROR, rootNode.get("status").get("errors").toString());
			}						

		} catch (Exception e) {
			// Ignore for now - Taxon didn't return a proper json result.
		}
		return exchange;
	}

}
