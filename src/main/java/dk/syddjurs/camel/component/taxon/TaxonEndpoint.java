/**
 * Copyright (C) 2014 Syddjurs Kommune
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.syddjurs.camel.component.taxon;

import org.apache.camel.Consumer;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.impl.DefaultPollingEndpoint;
import org.apache.camel.spi.UriParam;

/**
 * Taxon Endpoint
 */
public class TaxonEndpoint extends DefaultPollingEndpoint {
	@UriParam
	private final TaxonService taxonService;

	public TaxonEndpoint(String uri, TaxonComponent component, TaxonService taxonService) {
		super(uri, component);
		this.taxonService = taxonService;
	}

	@Override
	public Consumer createConsumer(Processor processor) throws Exception {
		throw new UnsupportedOperationException("This component does not support consuming from this endpoint");
	}

	@Override
	public Producer createProducer() throws Exception {
		return new TaxonProducer(this);
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public TaxonService getService() {
		return taxonService;
	}
}
