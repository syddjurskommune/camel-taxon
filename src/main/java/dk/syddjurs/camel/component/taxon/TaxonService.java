/**
 * Copyright (C) 2014 Syddjurs Kommune
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.syddjurs.camel.component.taxon;

import static org.apache.camel.util.ObjectHelper.notNull;

import java.io.IOException;

import org.apache.camel.spi.UriParam;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

public class TaxonService {

	private final String url;

	@UriParam
	private String taxonomy;
	@UriParam
	private String text;
	@UriParam
	private String subject;
	@UriParam
	private String settings;

	public TaxonService(TaxonComponent component, String url) {
		this.url = notNull(url, "url");
	}

	public String getUrl() {
		return url;
	}

	public String getTaxonomy() {
		return taxonomy;
	}

	public void setTaxonomy(String taxonomy) {
		this.taxonomy = taxonomy;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSettings() {
		return settings;
	}

	public void setSettings(String settings) {
		this.settings = settings;
	}

	public String invoke(String inputText) throws ClientProtocolException, IOException {
		return Request.Post(url).bodyForm(Form.form().add("taxonomy", taxonomy).add("text", inputText).add("settings", settings).build()).execute().returnContent().asString().trim();
	}

}
