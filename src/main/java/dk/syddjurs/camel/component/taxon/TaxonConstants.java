/**
 * Copyright (C) 2014 Syddjurs Kommune
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.syddjurs.camel.component.taxon;

/**
 * The Taxon constants
 */
public final class TaxonConstants {

	public static final String TAXON_STATUS = "CamelTaxonStatus";
	public static final String TAXON_ERROR = "CamelTaxonError";
	public static final String TAXON_CLASS = "CamelTaxonClass";
	public static final String TAXON_TITLE = "CamelTaxonTitle";
	public static final String TAXON_FULL_RESULT = "CamelTaxonFullResult";
	public static final String TAXON_SCORE_TOTAL = "CamelTaxonScoreTotal";
	public static final String TAXON_CONFIDENCE_COEFFICIENT = "CamelTaxonConfidenceCoefficient";

	private TaxonConstants() {
	}

}
