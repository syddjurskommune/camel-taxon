/**
 * Copyright (C) 2014 Syddjurs Kommune
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package camel.taxon;

import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

public class TaxonComponentTest extends CamelTestSupport {

	// TODO: persuade taxon.dk to host a test endpoint
	private final String endpointUrl = "http://taxon.syddjurs.dk/taxon-ws.php";
	private final String taxonomyPath = "../system/taxonomies/test_lookup.json";

	@EndpointInject(uri = "mock:result")
	protected MockEndpoint resultEndpoint;

	@Produce(uri = "direct:bodyRoute")
	protected ProducerTemplate tplBodyRoute;

	@Produce(uri = "direct:parmRoute")
	protected ProducerTemplate tplParmRoute;

	@Produce(uri = "direct:subjectRoute")
	protected ProducerTemplate tplSubjectRoute;

	/**
	 * "Denmark" is set in the body. The test checks for expected header values.
	 * Should return Scandinavia class with a high (100) ConfidenceCoefficent.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testClassifyBody() throws Exception {
		resultEndpoint.expectedHeaderReceived("CamelTaxonStatus","OK");
		resultEndpoint.expectedHeaderReceived("CamelTaxonClass", "01.02");
		resultEndpoint.expectedHeaderReceived("CamelTaxonTitle", "Scandinavia");
		resultEndpoint.expectedHeaderReceived("CamelTaxonScoreTotal", 15);
		resultEndpoint.expectedHeaderReceived("CamelTaxonConfidenceCoefficient", 100);

		tplBodyRoute.sendBody("Denmark");
		resultEndpoint.assertIsSatisfied();
	}

	/**
	 * Gemany is set in the body, but subject=Denmark is set in the endpoint
	 * parameter. Test checks for expected header values. Should return
	 * Scandinavia class, but with a lower ConfidenceCoefficent than a "pure"
	 * Denmark classification.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testClassifyBodyWithSubject() throws Exception {
		resultEndpoint.expectedHeaderReceived("CamelTaxonStatus","OK");
		resultEndpoint.expectedHeaderReceived("CamelTaxonClass", "01.02");
		resultEndpoint.expectedHeaderReceived("CamelTaxonTitle", "Scandinavia");
		resultEndpoint.expectedHeaderReceived("CamelTaxonScoreTotal", 15);
		resultEndpoint.expectedHeaderReceived("CamelTaxonConfidenceCoefficient", 66);

		tplSubjectRoute.sendBody("Germany");
		resultEndpoint.assertIsSatisfied();
	}

	/**
	 * Gemany is set in the body, but text=Denmark is set in the endpoint
	 * parameter. Test checks for expected header values. Should return
	 * Scandinavia class with a high (100) ConfidenceCoefficent (message body is
	 * ignored).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testClassifyParm() throws Exception {
		resultEndpoint.expectedHeaderReceived("CamelTaxonStatus","OK");
		resultEndpoint.expectedHeaderReceived("CamelTaxonClass", "01.02");
		resultEndpoint.expectedHeaderReceived("CamelTaxonTitle", "Scandinavia");
		resultEndpoint.expectedHeaderReceived("CamelTaxonScoreTotal", 15);
		resultEndpoint.expectedHeaderReceived("CamelTaxonConfidenceCoefficient", 100);

		tplParmRoute.sendBody("Germany");
		resultEndpoint.assertIsSatisfied();
	}

	/**
	 * Empty body. Test checks for expected header values. Should return error code 9010
	 * @throws Exception
	 */
	@Test
	public void testNoResult() throws Exception {
		resultEndpoint.expectedHeaderReceived("CamelTaxonStatus","Error");
		resultEndpoint.expectedHeaderReceived("CamelTaxonError","[{\"ErrorCode\":\"9010\",\"ErrorText\":\"Text is empty\"}]");
		tplBodyRoute.sendBody("");
		resultEndpoint.assertIsSatisfied();
	}

	@Override
	protected RouteBuilder createRouteBuilder() {
		return new RouteBuilder() {
			public void configure() {
				from("direct:bodyRoute")
				.to("taxon:" + endpointUrl + "?taxonomy=" + taxonomyPath)
				.log("CamelTaxonFullResult: ${in.header.CamelTaxonFullResult}")
				.to("mock:result");

				from("direct:parmRoute")
				.to("taxon:" + endpointUrl + "?taxonomy=" + taxonomyPath + "&text=Denmark")
				.log("CamelTaxonFullResult: ${in.header.CamelTaxonFullResult}")
				.to("mock:result");

				from("direct:subjectRoute")
				.to("taxon:" + endpointUrl + "?taxonomy=" + taxonomyPath + "&subject=Denmark")
				.log("CamelTaxonFullResult: ${in.header.CamelTaxonFullResult}")
				.to("mock:result");
			}
		};
	}
}
