Camel Taxon Component Project
=======================

This project is a Camel Taxon component.

To build this project use

	mvn install

Links

	http://camel.apache.org    
	http://www.taxon.dk/da/download
	https://bitbucket.org/syddjurskommune/camel-taxon
	